//
//  SlideFitRigestraion.swift
//  UICollectionZone
//
//  Created by Charlie Lam on 2020/6/12.
//  Copyright © 2020 CharlesLin. All rights reserved.
//

import Foundation
import UIKit

enum UserInfoPickerType: Int {
    case gender = 100
    case birthday = 101
//    case height = 102
//    case weight = 103
//    case bodyfat = 104
//    case goal = 105
    case unknown = 0
}

class SlideFitRegistrationView: UIView {
    @IBOutlet var avatarImageView: UIImageView!
    @IBOutlet var avatarBtn: UIButton!
    @IBOutlet var nameColumnView: UIView!
    @IBOutlet var nameLabelView: UIView!
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var genderLbl: UILabel!
    @IBOutlet var genderTextField: NoOptionTextField!
    @IBOutlet var birthdayLbl: UILabel!
    @IBOutlet var birthdayTextField: NoOptionTextField!
    @IBOutlet var nextStepBtn: NextStepButton!
    @IBOutlet var wordLimitLabel: UILabel!

    /// 性別選擇器
    lazy var genderPickerView: UIPickerView = {
        let pickerView = UIPickerView()
        return pickerView
    }()

    lazy var datePickerView = UIDatePicker()
    /// 生日選擇器
    lazy var birthdayPickerView: UIPickerView = {
        let pickerView = UIPickerView()
        return pickerView
    }()

    fileprivate var storeBirthday: Date = Date()
    /// 生日
    var birthday: Date {
        set {
            storeBirthday = newValue
            let today = Date()
            let differenceComponents = Calendar.current.dateComponents([.year], from: newValue, to: today)
//            let age: Int = differenceComponents.year ?? 10
//            let languageCode = filterCurrentLanguageCode()
//            if languageCode == "zh-Hant" || languageCode == "zh-Hans" {
//                birthdayTextField.text = "\(newValue.dateDateLongLocaleString) (\(age))"
//            } else {
//                birthdayTextField.text = "\(newValue.dateMMMddSpaceyyyyLocaleString) (\(age))"
//            }
            birthdayTextField.text = "\(newValue.dateMMMddSpaceyyyyLocaleString)"
        }
        get {
            storeBirthday
        }
    }

    /// 顯示選擇性別
    ///
    /// - Parameter sender: UITextField
    func showGenderPickerEditing(sender: UITextField) {
        genderPickerView.tag = UserInfoPickerType.gender.rawValue
        genderPickerView.reloadAllComponents()
        genderPickerView.selectRow(0, inComponent: 0, animated: false)
        sender.inputView = genderPickerView
    }

    /// 顯示選擇出生年月日
    ///
    /// - Parameter sender: UITextField
    func showBirthdayDatePickerEditing(sender: UITextField) {
        datePickerView.datePickerMode = .date
        datePickerView.date = DEFAULT_DATE
        datePickerView.tag = UserInfoPickerType.birthday.rawValue
        sender.inputView = datePickerView
    }

    override func awakeFromNib() {
        setupView()
    }

    func lightenNameColumn() {
        nameLabelView.isHidden = false
        nameColumnView.layer.borderColor = AppColor.yellowyGreen.cgColor
        nameLbl.textColor = AppColor.yellowyGreen
    }

    @objc func dismissKeyboard() {
        nameColumnView.layer.borderColor = AppColor.C283C47.cgColor
        nameLbl.textColor = AppColor.C283C47
        // 關閉鍵盤
        endEditing(true)
    }
}

// MARK: - Private Methods

private extension SlideFitRegistrationView {
    func setupView() {
        nameColumnView.layer.masksToBounds = true
        nameColumnView.layer.borderWidth = 1
        nameColumnView.layer.borderColor = AppColor.C283C47.cgColor

        // 加入預設點擊無事件處理部分，關閉鍵盤
        let ges = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        addGestureRecognizer(ges)
        
        nameTextField.attributedPlaceholder = NSAttributedString(
            string: "wondercise_sign_register_email_input_email_hint".localized,
            attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.4196078431, green: 0.3882352941, blue: 0.4549019608, alpha: 1), NSAttributedString.Key.font: UIFont(name: "PingFangTC-Regular", size: 18) ?? ""]
        )

        nameTextField.attributedPlaceholder = NSAttributedString(
            string: "Enter Your Name and Surname".localized,
            attributes: [NSAttributedString.Key.foregroundColor: AppColor.C283C47.cgColor]
        )

        genderTextField.attributedPlaceholder = NSAttributedString(
            string: "Choose Your Gender".localized,
            attributes: [NSAttributedString.Key.foregroundColor: AppColor.C283C47.cgColor]
        )

        birthdayTextField.attributedPlaceholder = NSAttributedString(
            string: "Select Your Date of Birth".localized,
            attributes: [NSAttributedString.Key.foregroundColor: AppColor.C283C47.cgColor]
        )
    }
}

let DEFAULT_DATE = Date(timeIntervalSince1970: 157_788_000)

/** 複寫UITextField 關閉複製貼上 */
class NoOptionTextField: UITextField {
    init() {
        super.init(frame: .zero)
    }

    override func canPerformAction(_: Selector, withSender _: Any?) -> Bool {
        false
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

class RoundedCornerButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height * 0.5
    }
}

@IBDesignable class NextStepButton: RoundedCornerButton {
    @IBInspectable var isAllowedToProceed: Bool = false {
        didSet {
            if isAllowedToProceed {
                isEnabled = true
                layer.backgroundColor = AppColor.yellowyGreen.cgColor
                tintColor = .black
                setTitleColor(.black, for: .normal)
            } else {
                isEnabled = false
                layer.backgroundColor = AppColor.mossyGreen.cgColor
                tintColor = #colorLiteral(red: 0.1764705882, green: 0.1607843137, blue: 0.1960784314, alpha: 1)
                setTitleColor(#colorLiteral(red: 0.1764705882, green: 0.1607843137, blue: 0.1960784314, alpha: 1), for: .normal)
            }
        }
    }
}

enum AppColor {
    static let yellowyGreen = #colorLiteral(red: 0.8352941176, green: 0.9058823529, blue: 0.2235294118, alpha: 1)
    static let mossyGreen = #colorLiteral(red: 0.431372549, green: 0.4588235294, blue: 0.1137254902, alpha: 1)
    static let C283C47 = #colorLiteral(red: 0.1568627451, green: 0.2352941176, blue: 0.2784313725, alpha: 1)
}

extension Date {
    var dateMMMddSpaceyyyyLocaleString: String {
        Formatter.dateMMMddSpaceyyyyLocale.string(from: self)
    }
}

extension Formatter {
    static let dateMMMddSpaceyyyyLocale: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
//        formatter.locale = Locale(identifier: filterCurrentLanguageCode())
//        formatter.dateFormat = "MMM dd, yyyy"
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }()
}

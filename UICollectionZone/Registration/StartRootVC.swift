//
//  StartRootViewViewController.swift
//  UICollectionZone
//
//  Created by Charlie Lam on 2020/6/15.
//  Copyright © 2020 CharlesLin. All rights reserved.
//

import UIKit

class StartRootVC: UIViewController {

    lazy var startRootView: StartRootView = {
        let nib = UINib(nibName: "\(StartRootView.self)", bundle: Bundle(for: StartRootView.self))
        let view = nib.instantiate(withOwner: nil).first { $0 is StartRootView } as! StartRootView
        view.startButton.addTarget(
            self,
            action: #selector(StartRootVC.start(button:)),
            for: .touchUpInside
        )
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(startRootView)
        startRootView.snp.makeConstraints { maker in
            maker.edges.equalToSuperview()
        }
        // Do any additional setup after loading the view.
    }
    
    @objc func start(button _: UIButton) {
        self.performSegue(withIdentifier: "showProfile", sender: self)
    }
}

//
//  ActionBaseContainerVC.swift
//  WonderCoreFit
//
//  Created by Jen Che Lai on 2019/12/4.
//  Copyright © 2019 WonderCore. All rights reserved.
//

import UIKit

/// 如有view要響應SafeArea的值作改變，可實踐此protocol
protocol SafeAreaInsetsRespondable {
    func updateSafeAreaInsets(_ aUIEdgeInsets: UIEdgeInsets)
}

class ActionBaseContainerVC: UIViewController {
    var contentView: UIView?

    var tapRecognizer: UITapGestureRecognizer?

    init(contentView: UIView) {
        super.init(nibName: nil, bundle: nil)
        modalPresentationStyle = .custom
        layoutContent(contentView: contentView)
        self.contentView = contentView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupGestureRecognizers()
    }

    required init(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /// 更改contentView layout的method
    ///
    /// - Parameter contentView: 被放進container的view
    func layoutContent(contentView _: UIView) {}
}

// MARK: - UIGestureRecognizerDelegate

extension ActionBaseContainerVC: UIGestureRecognizerDelegate {
    func gestureRecognizer(_: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        touch.view === view
    }
}

// MARK: - Private Methods

private extension ActionBaseContainerVC {
    func setupGestureRecognizers() {
        let gestureRecognizer = UITapGestureRecognizer(target: self,
                                                       action: #selector(handleTap(gestureRecognizer:)))
        gestureRecognizer.delegate = self
        view.addGestureRecognizer(gestureRecognizer)
        tapRecognizer = gestureRecognizer
    }

    @objc func handleTap(gestureRecognizer _: UITapGestureRecognizer) {
        dismiss(animated: true)
    }
}

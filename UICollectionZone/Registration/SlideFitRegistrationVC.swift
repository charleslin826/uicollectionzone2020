//
//  SlideFitRegistrationVC.swift
//  UICollectionZone
//
//  Created by Charlie Lam on 2020/6/12.
//  Copyright © 2020 CharlesLin. All rights reserved.
//

import Foundation
import PromiseKit
import SnapKit
import UIKit

class SlideFitRegistrationVC: UIViewController {
    struct BasicProfileInputState: OptionSet {
        let rawValue: Int
        static let gender = BasicProfileInputState(rawValue: 1 << 0)
        static let birthday = BasicProfileInputState(rawValue: 1 << 1)
    }

    fileprivate var state: BasicProfileInputState = []

    fileprivate let imagePicker = UIImagePickerController()

    fileprivate let mActionSheetTransitioningDelegate = ActionSheetTransitioningDelegate()

    /// 使用者的大頭照
    var userImageURLString: String?

    fileprivate lazy var maxNameLenthStr = ""

    lazy var slideFitRegistrationView: SlideFitRegistrationView = {
        let nib = UINib(nibName: "\(SlideFitRegistrationView.self)", bundle: Bundle(for: SlideFitRegistrationView.self))
        let view = nib.instantiate(withOwner: nil).first { $0 is SlideFitRegistrationView } as! SlideFitRegistrationView
        view.nameTextField.delegate = self
        view.genderTextField.delegate = self
        view.birthdayTextField.delegate = self
        view.genderPickerView.dataSource = self
        view.genderPickerView.delegate = self
        view.avatarBtn.addTarget(
            self,
            action: #selector(SlideFitRegistrationVC.chooseAvatar(button:)),
            for: .touchUpInside
        )
        view.nameTextField.addTarget(
            self,
            action: #selector(SlideFitRegistrationVC.textFieldDidChange(_:)),
            for: .editingChanged
        )
        view.datePickerView.addTarget(
            self,
            action: #selector(SlideFitRegistrationVC.datePickerValueChanged(sender:)),
            for: .valueChanged
        )
        view.nextStepBtn.addTarget(
            self,
            action: #selector(SlideFitRegistrationVC.pressedNext(button:)),
            for: .touchUpInside
        )
        return view
    }()

    @objc func datePickerValueChanged(sender: UIDatePicker) {
        slideFitRegistrationView.birthday = sender.date
    }

    override func viewDidLoad() {
        super.viewDidLoad()
//        view.addSubview(slideFitRegistrationView)
//        slideFitRegistrationView.snp.makeConstraints { maker in
//            maker.edges.equalToSuperview()
//        }

//        if #available(iOS 13.0, *) {
//            let barAppearance = UINavigationBarAppearance()
//            barAppearance.configureWithTransparentBackground()
//            navigationController?.navigationBar.standardAppearance = barAppearance
//        } else {
//            let image = UIImage()
//            navigationController?.navigationBar.setBackgroundImage(image, for: .default)
//        }
    }
}

// MARK: - Actions

extension SlideFitRegistrationVC {
    /** 下一步 */
    @objc func pressedNext(button _: UIButton) {
        print("Next")
        performSegue(withIdentifier: "showBasicData", sender: self)
    }

    /// 顯示大頭照的相關動作
    ///
    /// - Parameter button: UIButton
    @objc func chooseAvatar(button _: UIButton) {
        let photoActionView = PhotoActionView()
        photoActionView.cancelBtn.addTarget(
            self,
            action: #selector(SlideFitRegistrationVC.dismissPhotoAction(button:)),
            for: .touchUpInside
        )
        photoActionView.takePhotoBtn.addTarget(
            self,
            action: #selector(SlideFitRegistrationVC.takePhoto(button:)),
            for: .touchUpInside
        )
        photoActionView.chooseLibraryBtn.addTarget(
            self,
            action: #selector(SlideFitRegistrationVC.chooseFromLibrary(button:)),
            for: .touchUpInside
        )
        photoActionView.removePhotoBtn.addTarget(
            self,
            action: #selector(SlideFitRegistrationVC.removeCurrentPhoto(button:)),
            for: .touchUpInside
        )
        let photoActionController = ActionSheetContainerVC(contentView: photoActionView)
        photoActionController.transitioningDelegate = mActionSheetTransitioningDelegate
        var bottomInset: CGFloat = 0
        if #available(iOS 11.0, *) {
            bottomInset = view.safeAreaInsets.bottom
        }
        photoActionView.snp.makeConstraints { maker in
            // 77 = cancelButton base height(42) + gap height(27) + top padding(8)
            maker.height.equalTo(77 + 50 * 3 + bottomInset)
            maker.bottom.left.right.equalToSuperview()
        }
        present(photoActionController, animated: true, completion: nil)
    }

    @objc func dismissPhotoAction(button _: UIButton) {
        print("dismissPhotoAction")
        presentedViewController?.dismiss(animated: true, completion: nil)
    }

    @objc func takePhoto(button _: UIButton) {
        print("takePhoto")
        presentedViewController?.dismiss(animated: true, completion: nil)
        firstly {
            PermissionConstants.checkCameraPermission(vc: self)
        }.done { bool -> Void in
            if bool {
                self.setupImagePicker()
            }
        }
    }

    // 從照片機取得照片
    fileprivate func setupImagePicker() {
        firstly { () -> Promise<[UIImagePickerController.InfoKey: Any]> in
            imagePicker.sourceType = .camera
            imagePicker.cameraCaptureMode = .photo
            imagePicker.modalPresentationStyle = .fullScreen
            return promise(imagePicker, animate: .disappear, completion: nil)
        }.done { infoKey -> Void in
            guard let image = infoKey[.originalImage] as? UIImage else { return }
            let size = CGSize(width: 500, height: 500)
            let cropImage = image.crop(to: size)
            self.slideFitRegistrationView.avatarImageView.image = cropImage
        }.catch { _ in
            //            ELog("SlideFitRegistrationVC : takePhoto", error)
            //            self.exceptionAlert(error: error)
        }
    }

    @objc func removeCurrentPhoto(button _: UIButton) {
        print("removeCurrentPhoto")
        presentedViewController?.dismiss(animated: true, completion: nil)
        if #available(iOS 13.0, *) {
            slideFitRegistrationView.avatarImageView.image = PicConstants.userDefaultPic
        } else {
            // Fallback on earlier versions
        }
    }

    // 從相簿取得照片
    @objc func chooseFromLibrary(button _: UIButton) {
        presentedViewController?.dismiss(animated: true, completion: nil)
        firstly { () -> Promise<[UIImagePickerController.InfoKey: Any]> in
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            self.imagePicker.modalPresentationStyle = .fullScreen
            return self.promise(self.imagePicker, animate: .disappear, completion: nil)
        }.done { infoKey -> Void in
            guard let image = infoKey[.originalImage] as? UIImage else { return }
            let size = CGSize(width: 500, height: 500)
            let cropImage = image.crop(to: size)

            self.slideFitRegistrationView.avatarImageView.image = cropImage

            //                let url = URL(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true)
            //                    .appendingPathComponent("coverImage", isDirectory: false)
            //                    .appendingPathExtension("jpg")
            //
            //                // Then write to disk
            //                if let data = UIImageJPEGRepresentation(cropImage, 0.7) {
            //                    do {
            //                        try data.write(to: url, options: .atomic)
            //                        self.tempCustomImageUrl = url
            //                    } catch {
            //                        print("Handle the error, i.e. disk can be full")
            //                        self.tempCustomImageUrl = nil
            //                    }
            //                }
        }.catch { _ in
            //            ELog("BaseProfileVC : chooseFromLibrary", error)
            //            self.exceptionAlert(error: error)
        }
    }

    class PhotoActionView: UIView, SafeAreaInsetsRespondable {
        lazy var cancelBtn = UIButton(type: .custom)
        lazy var removePhotoBtn = UIButton()
        lazy var chooseLibraryBtn = UIButton()
        lazy var takePhotoBtn = UIButton()
        lazy var buttonHeight = 50

        func updateSafeAreaInsets(_ aUIEdgeInsets: UIEdgeInsets) {
            cancelBtn.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: aUIEdgeInsets.bottom, right: 0)
            cancelBtn.snp.updateConstraints { maker in
                maker.height.equalTo(CGFloat(42) + aUIEdgeInsets.bottom)
            }
        }

        init() {
            super.init(frame: .zero)
            backgroundColor = UIColor.white
            addSubview(cancelBtn)
            let gapView = UIView()
            addSubview(gapView)
            addSubview(removePhotoBtn)
            addSubview(chooseLibraryBtn)
            addSubview(takePhotoBtn)

            cancelBtn.backgroundColor = UIColor.white
            cancelBtn.setTitle("wondercise_sign_profile_picture_cancel".localized, for: .normal)
            cancelBtn.titleLabel?.font = UIFont(name: "Helvetica", size: 15)
            //            cancelBtn.setTitleColor(AppColor.onehundredGrey, for: .normal)

            cancelBtn.snp.makeConstraints { maker in
                maker.height.equalTo(42)
                maker.left.right.bottom.equalToSuperview()
            }

            let divider = UIView()
            //            divider.backgroundColor = AppColor.veryLightPinkTwo
            cancelBtn.addSubview(divider)
            divider.snp.makeConstraints { maker in
                maker.top.left.right.equalToSuperview()
                maker.height.equalTo(0.5)
            }

            gapView.backgroundColor = UIColor.white
            gapView.snp.makeConstraints { maker in
                maker.bottom.equalTo(cancelBtn.snp.top)
                maker.height.equalTo(27)
                maker.left.right.equalToSuperview()
            }

            removePhotoBtn.backgroundColor = UIColor.white
            //            removePhotoBtn.setImage(#imageLiteral(resourceName: "delete"), for: .normal)
            removePhotoBtn.imageView?.contentMode = .left
            removePhotoBtn.setTitle("wondercise_sign_profile_picture_remove".localized, for: .normal)
            removePhotoBtn.titleLabel?.font = UIFont(name: "Helvetica", size: 16)
            removePhotoBtn.setTitleColor(#colorLiteral(red: 0.399423033, green: 0.399748534, blue: 0.3994735479, alpha: 1), for: .normal)
            removePhotoBtn.imageView?.snp.makeConstraints { maker in
                maker.left.equalToSuperview().offset(7)
                maker.top.bottom.equalToSuperview()
            }
            removePhotoBtn.titleLabel?.snp.makeConstraints { maker in
                maker.left.equalToSuperview().offset(38)
                maker.top.bottom.equalToSuperview()
            }
            removePhotoBtn.snp.makeConstraints { maker in
                maker.bottom.equalTo(gapView.snp.top)
                maker.height.equalTo(buttonHeight)
                maker.left.right.equalToSuperview()
            }

            chooseLibraryBtn.backgroundColor = UIColor.white
            //            chooseLibraryBtn.setImage(#imageLiteral(resourceName: "timewarp_library"), for: .normal)
            chooseLibraryBtn.imageView?.contentMode = .left
            chooseLibraryBtn.setTitle("wondercise_sign_profile_picture_choose".localized, for: .normal)
            chooseLibraryBtn.titleLabel?.font = UIFont(name: "Helvetica", size: 16)
            chooseLibraryBtn.setTitleColor(#colorLiteral(red: 0.399423033, green: 0.399748534, blue: 0.3994735479, alpha: 1), for: .normal)
            chooseLibraryBtn.imageView?.snp.makeConstraints { maker in
                maker.left.equalToSuperview().offset(7)
                maker.top.bottom.equalToSuperview()
            }
            chooseLibraryBtn.titleLabel?.snp.makeConstraints { maker in
                maker.left.equalToSuperview().offset(38)
                maker.top.bottom.equalToSuperview()
            }
            chooseLibraryBtn.snp.makeConstraints { maker in
                maker.bottom.equalTo(removePhotoBtn.snp.top)
                maker.height.equalTo(buttonHeight)
                maker.left.right.equalToSuperview()
            }

            takePhotoBtn.backgroundColor = UIColor.white
            //            takePhotoBtn.setImage(#imageLiteral(resourceName: "timewarp_photo"), for: .normal)
            takePhotoBtn.imageView?.contentMode = .left
            takePhotoBtn.setTitle("wondercise_sign_profile_picture_takephoto".localized, for: .normal)
            takePhotoBtn.titleLabel?.font = UIFont(name: "Helvetica", size: 16)
            takePhotoBtn.setTitleColor(#colorLiteral(red: 0.399423033, green: 0.399748534, blue: 0.3994735479, alpha: 1), for: .normal)
            takePhotoBtn.imageView?.snp.makeConstraints { maker in
                maker.left.equalToSuperview().offset(7)
                maker.top.bottom.equalToSuperview()
            }
            takePhotoBtn.titleLabel?.snp.makeConstraints { maker in
                maker.left.equalToSuperview().offset(38)
                maker.top.bottom.equalToSuperview()
            }
            takePhotoBtn.snp.makeConstraints { maker in
                maker.bottom.equalTo(chooseLibraryBtn.snp.top)
                maker.height.equalTo(buttonHeight)
                maker.left.right.equalToSuperview()
            }
        }

        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
        }
    }
}

// MARK: - PickerView DataSource

extension SlideFitRegistrationVC: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        let type: UserInfoPickerType = UserInfoPickerType(rawValue: pickerView.tag) ?? UserInfoPickerType.unknown
        switch type {
        case .gender:
            return 1
        default:
            return 0
        }
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent _: Int) -> Int {
        let type: UserInfoPickerType = UserInfoPickerType(rawValue: pickerView.tag) ?? UserInfoPickerType.unknown
        switch type {
        case .gender:
            return genderList.count
        default:
            return 0
        }
    }
}

// MARK: - PickerView Delegate

extension SlideFitRegistrationVC: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent _: Int) -> String? {
        let type: UserInfoPickerType = UserInfoPickerType(rawValue: pickerView.tag) ?? UserInfoPickerType.unknown
        switch type {
        case .gender:
            return genderList[row]
        default:
            return ""
        }
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent _: Int) {
        let type: UserInfoPickerType = UserInfoPickerType(rawValue: pickerView.tag) ?? UserInfoPickerType.unknown
        switch type {
        case .gender:
            slideFitRegistrationView.genderTextField.text = genderList[row]
        default:
            break
        }
    }
}

// MARK: - TextField Delegate

extension SlideFitRegistrationVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case slideFitRegistrationView.nameTextField:
            textField.resignFirstResponder()
        default:
            break
        }
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case slideFitRegistrationView.nameTextField:
            slideFitRegistrationView.lightenNameColumn()
        default:
            break
        }
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField {
        case slideFitRegistrationView.genderTextField:
            slideFitRegistrationView.showGenderPickerEditing(sender: textField)
            if let text = slideFitRegistrationView.genderTextField.text, text.isEmpty {
                slideFitRegistrationView.genderTextField.text = "female" // "wondercise_common_female".localized
                state.insert(.gender)
                checkFillState()
            }
        case slideFitRegistrationView.birthdayTextField:
            slideFitRegistrationView.showBirthdayDatePickerEditing(sender: textField)
            if let text = slideFitRegistrationView.birthdayTextField.text, text.isEmpty {
                slideFitRegistrationView.birthday = DEFAULT_DATE
                state.insert(.birthday)
                checkFillState()
            }
        default:
            break
        }
        return true
    }

    @objc func textFieldDidChange(_: UITextField) {
        if var count = slideFitRegistrationView.nameTextField.text?.count {
            if count >= 20 {
                print("字數超過20")
                if count == 20 {
                    if let str = slideFitRegistrationView.nameTextField.text {
                        maxNameLenthStr = str
                    }
                }
                if count > 20 {
                    count = 20
                    slideFitRegistrationView.nameTextField.text = maxNameLenthStr
                }
            }
            slideFitRegistrationView.wordLimitLabel.text = "\(count)/20"
        }
        checkFillState()
    }
}

// MARK: - Private Methods

private extension SlideFitRegistrationVC {
    func checkFillState() {
        let isNameTextFieldEmpty: Bool = slideFitRegistrationView.nameTextField.text?.isEmpty ?? true
        slideFitRegistrationView.nextStepBtn.isAllowedToProceed = !isNameTextFieldEmpty && state.contains(.gender) && state.contains(.birthday)
    }
}

// AppConstants

/// 性別選擇列表
let genderList = ["Female", "Male"] // ["wondercise_common_female".localized, "wondercise_common_male".localized]
enum PicConstants {
    @available(iOS 13.0, *)
    static let userDefaultPic = UIImage(systemName: "house", withConfiguration: nil)
}

//
//  StartRootView.swift
//  UICollectionZone
//
//  Created by Charlie Lam on 2020/6/15.
//  Copyright © 2020 CharlesLin. All rights reserved.
//

import Foundation
import UIKit

class StartRootView: UIView {
    @IBOutlet var startButton: UIButton!

    override func awakeFromNib() {
        setupView()
    }
}

// MARK: - Private Methods

private extension StartRootView {
    func setupView() {
        startButton.setTitle("START", for: .normal)
    }
}

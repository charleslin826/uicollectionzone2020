//
//  ActionSheetContainerVC.swift
//  WonderCoreFit
//
//  Created by Jen Che Lai on 2019/9/2.
//  Copyright © 2019 WonderCore. All rights reserved.
//

import UIKit

/// 用來呈現自定義的Action Sheet
class ActionSheetContainerVC: ActionBaseContainerVC {
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if #available(iOS 11.0, *) {
            let inset = view.safeAreaInsets
            view.subviews.filter {
                $0 is SafeAreaInsetsRespondable
            }.forEach {
                ($0 as! SafeAreaInsetsRespondable).updateSafeAreaInsets(inset)
            }
        }
    }

    override func layoutContent(contentView: UIView) {
        view.addSubview(contentView)
        contentView.snp.makeConstraints { maker in
            maker.left.bottom.right.equalToSuperview()
        }
    }

    deinit {
        DLog("\(type(of: self)) deinit")
    }
}

/// ActionSheetTransitioningDelegate
class ActionSheetTransitioningDelegate: NSObject, UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source _: UIViewController) -> UIPresentationController? {
        let presentationController = DimmedViewPresentationController(presentedViewController: presented, presenting: presenting)
        return presentationController
    }

    func animationController(forPresented _: UIViewController, presenting _: UIViewController, source _: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let animationController = ActionSheetAnimator()
        animationController.isPresenting = true
        return animationController
    }

    func animationController(forDismissed _: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let animationController = ActionSheetAnimator()
        animationController.isPresenting = false
        return animationController
    }
}

/// DimmedViewPresentationController
class DimmedViewPresentationController: UIPresentationController {
    fileprivate lazy var dimmedView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        dimmedView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.7)
        dimmedView.alpha = 0.0
    }

    override func presentationTransitionWillBegin() {
        guard let unwrappedContainerView = containerView else {
            return
        }
        dimmedView.alpha = 0.0
        unwrappedContainerView.insertSubview(dimmedView, at: 0)
        dimmedView.snp.makeConstraints { maker in
            maker.edges.equalToSuperview()
        }
        guard let coordinator = presentedViewController.transitionCoordinator else {
            dimmedView.alpha = 1.0
            return
        }
        coordinator.animate(alongsideTransition: { _ in
            self.dimmedView.alpha = 1.0
        })
    }

    override func dismissalTransitionWillBegin() {
        guard let coordinator = presentedViewController.transitionCoordinator else {
            dimmedView.alpha = 0.0
            return
        }
        coordinator.animate(alongsideTransition: { _ in
            self.dimmedView.alpha = 0.0
        })
    }

    override func containerViewWillLayoutSubviews() {
        guard let unwrappedContainerView = containerView else {
            return
        }
        presentedView?.frame = unwrappedContainerView.bounds
    }

    override var shouldPresentInFullscreen: Bool {
        true
    }
}

/// ActionSheetAnimator
class ActionSheetAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    var isPresenting = false

    func transitionDuration(using _: UIViewControllerContextTransitioning?) -> TimeInterval {
        0.5
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromViewController = transitionContext.viewController(forKey: .from),
            let toViewController = transitionContext.viewController(forKey: .to),
            let fromView = fromViewController.view,
            let toView = toViewController.view else {
            return
        }
        let containerView = transitionContext.containerView
        if isPresenting {
            containerView.addSubview(toView)
        }
        let animatingViewController =
            isPresenting ? toViewController : fromViewController
        let animatingView = animatingViewController.view
        let appearedFrame = transitionContext.finalFrame(for: animatingViewController)
        var dismissedFrame = appearedFrame
        dismissedFrame.origin.y += dismissedFrame.size.height
        let initialFrame = isPresenting ? dismissedFrame : appearedFrame
        let finalFrame = isPresenting ? appearedFrame : dismissedFrame
        animatingView?.frame = initialFrame
        UIView.animate(
            withDuration: transitionDuration(using: transitionContext),
            delay: 0.0,
            usingSpringWithDamping: 300.0,
            initialSpringVelocity: 5.0,
            options: [.allowUserInteraction, .beginFromCurrentState],
            animations: {
                animatingView?.frame = finalFrame
            }, completion: { _ in
                if !self.isPresenting {
                    fromView.removeFromSuperview()
                }
                transitionContext.completeTransition(true)
            }
        )
    }
}

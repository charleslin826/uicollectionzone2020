//
//  DetailViewController.swift
//  UICollectionZone
//
//  Created by Charlie Lam on 2020/6/12.
//  Copyright © 2020 CharlesLin. All rights reserved.
//

import PromiseKit
import SnapKit
import UIKit

class DetailViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()

//        if #available(iOS 13.0, *) {
//            let barAppearance = UINavigationBarAppearance()
//            barAppearance.configureWithTransparentBackground()
//            navigationController?.navigationBar.standardAppearance = barAppearance
//        } else {
//            let image = UIImage()
//            navigationController?.navigationBar.setBackgroundImage(image, for: .default)
//        }
    }

    fileprivate func addChildVC(_ vc: UIViewController) {
        //            let navigationController = UINavigationController(rootViewController: vc)
        //            navigationController.modalPresentationStyle = .fullScreen
        //            present(navigationController, animated: true, completion: nil)
        
        let childNavigation = UINavigationController(rootViewController: vc)
        childNavigation.willMove(toParent: self)
        addChild(childNavigation)
        childNavigation.view.frame = view.frame
        view.addSubview(childNavigation.view)
        childNavigation.didMove(toParent: self)
    }
    
    func configureView() {
        if selectedId == 1 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "SlideFitRegistrationVC")
            guard let vc = viewController as? SlideFitRegistrationVC else {
                return
            }
            addChildVC(vc)

        } else if selectedId == 2 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "WonderciseChannelTopVC")
            guard let vc = viewController as? WonderciseChannelTopVC else {
                return
            }
            addChildVC(vc)
        } else {
            print("selectedId : \(selectedId)")
        }
    }
}

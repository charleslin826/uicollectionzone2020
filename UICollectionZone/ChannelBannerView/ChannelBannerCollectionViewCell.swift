//
//  ChannelVideoListTopView.swift
//  WonderCoreFit
//
//  Created by Charlie Lam on 2020/8/27.
//  Copyright © 2020 WonderCore. All rights reserved.
//

import UIKit

class ChannelBannerCollectionViewCell: UICollectionViewCell {
    @IBOutlet var frameViewHeightConstraint: NSLayoutConstraint!
//    @IBOutlet var videoView: UIView!
    @IBOutlet var imageViewTopConstraint: NSLayoutConstraint!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var titleSectionView: UIView!
    @IBOutlet var titleText: UILabel!
    @IBOutlet var strength: UILabel!
    @IBOutlet var videoNumber: UILabel!
    @IBOutlet var videoLabel: UILabel!
    @IBOutlet var gradientBottomView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }

    // for autolayout
    private lazy var width: NSLayoutConstraint = {
        let width = contentView.widthAnchor.constraint(equalToConstant: bounds.size.width)
        width.isActive = true
        return width
    }()

    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        width.constant = bounds.size.width
        print("systemLayoutSizeFitting , bounds.size.width \(bounds.size.width)")
        return contentView.systemLayoutSizeFitting(CGSize(width: targetSize.width, height: 1))
    }

//    func configure(_ vm: ChannelVideoListViewModel.BannerCellModel) {
//        settingImage(vm.imageUrl)
//        titleText.text = vm.name
//        let videoCount = vm.numberOfVideos
//        videoNumber.text = "\(videoCount)"
//        videoLabel.text = AppUtility.videoUnitString(number: videoCount)
//        levelView.rating = vm.strength
//
//        strength.text = "wondercise_common_unit_training_strength".localized
//        videoLabel.text = "wondercise_common_unit_videos".localized
//    }
}

// MARK: - Private Methods

private extension ChannelBannerCollectionViewCell {
    func setupView() {
        videoNumber.font = UIFont(name: "BebasNeue", size: 17)
    }
//
//    func settingImage(_ imgURL: String) {
//        if let imageUrl = URL(string: imgURL) {
//            let imageId = "\(imgURL)_thumb"
//            AppUtility.fetchImage(imageId: imageId, url: imageUrl) { hasResult, resultImage in
//                if hasResult, let unwrappedImage = resultImage {
//                    self.imageView.contentMode = .scaleAspectFill
//                    self.imageView.image = unwrappedImage
//                }
//            }
//        }
//    }
}

//
//  WonderciseChannelTopVC.swift
//  UICollectionZone
//
//  Created by Charlie Lam on 2020/9/9.
//  Copyright © 2020 CharlesLin. All rights reserved.
//

import UIKit

class WonderciseChannelTopVC: UIViewController {
    fileprivate var customNaviHeight = CGFloat(64)
    fileprivate let switchViewHeight = CGFloat(55)
    var topView: ChannelBannerCollectionViewCell?
    lazy var wonderciseChannelTopView: WonderciseChannelTopView = {
        let nib = UINib(nibName: "\(WonderciseChannelTopView.self)", bundle: Bundle(for: WonderciseChannelTopView.self))
        let view = nib.instantiate(withOwner: nil).first { $0 is WonderciseChannelTopView } as! WonderciseChannelTopView
        view.collectionView.register(
            UINib(nibName: "\(ChannelBannerCollectionViewCell.self)",
                  bundle: Bundle(for: ChannelBannerCollectionViewCell.self)),
            forCellWithReuseIdentifier: "CellIdentifier.channelBannerCollectionViewCell"
        )
        view.collectionView.register(
            UINib(nibName: "\(ChannelDescriptionCollectionViewCell.self)",
                  bundle: Bundle(for: ChannelDescriptionCollectionViewCell.self)),
            forCellWithReuseIdentifier: "CellIdentifier.channelDescriptionCollectionViewCell"
        )
        view.collectionView.register(
            UINib(nibName: "\(ChannelTabCollectionViewCell.self)",
                  bundle: Bundle(for: ChannelTabCollectionViewCell.self)),
            forCellWithReuseIdentifier: "CellIdentifier.channelTabCollectionViewCell"
        )
        view.collectionView.register(
            UINib(nibName: "\(VideosTitleCollectionViewCell.self)",
                  bundle: Bundle(for: VideosTitleCollectionViewCell.self)),
            forCellWithReuseIdentifier: "CellIdentifier.videosTitleCollectionViewCell"
        )
        view.collectionView.register(
            UINib(nibName: "\(ChannelVideoCollectionViewCell.self)",
                  bundle: Bundle(for: ChannelVideoCollectionViewCell.self)),
            forCellWithReuseIdentifier: "CellIdentifier.channelVideoCollectionViewCell"
        )

        view.collectionView.contentInsetAdjustmentBehavior = .never
        view.collectionView.delegate = self
        view.collectionView.dataSource = self
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        wonderciseChannelTopView.collectionView.reloadData()
    }

    override func viewDidAppear(_: Bool) {
        wonderciseChannelTopView.collectionView.reloadItems(at: [IndexPath(row: 0, section: 0)])
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        // view.safeAreaInsets will wierd when nav from DetailVC, so you need to change initial VC to this VC to gain better experience.
        print("view.safeAreaInsets \(view.safeAreaInsets)")
        customNaviHeight = CGFloat(44) + view.safeAreaInsets.top
//        topView?.frameViewHeightConstraint.constant = -customNaviHeight
        topView?.imageViewTopConstraint.constant = -customNaviHeight
    }
}

private extension WonderciseChannelTopVC {
    func setupView() {
        view.addSubview(wonderciseChannelTopView)
        wonderciseChannelTopView.translatesAutoresizingMaskIntoConstraints = false
        wonderciseChannelTopView.snp.makeConstraints { maker in
            maker.edges.equalToSuperview()
        }
    }

    // MARK: 表格上方的滑動特效控制

    func topViewBouncingEffect(_ offsetY: CGFloat, _ topView: ChannelBannerCollectionViewCell) {
        if offsetY > 0.0 {
            topView.imageViewTopConstraint.constant = 0 - customNaviHeight
        } else {
            topView.imageViewTopConstraint.constant = offsetY - customNaviHeight
            topView.imageView.layer.transform = CATransform3DIdentity
        }
    }
}

// MARK: - UIScrollViewDelegate

extension WonderciseChannelTopVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let topView = topView else { return }
        let offsetY: CGFloat = scrollView.contentOffset.y
//        print("scrollViewDidScroll \(offsetY)")
//        scrollOffsetY = offsetY
        topViewBouncingEffect(offsetY, topView)
//        navigationBarAlphaEffect(scrollView, topView)
//
//        guard mChannelVideoListViewModel.channelVideoListCellViewTypeList.value.contains(.tab) else { return }
//        let indexPath = IndexPath(row: tabRow, section: 0)
//        guard let theAttributes = channelVideoListView.collectionView.collectionViewLayout.layoutAttributesForItem(at: indexPath) else {
//            return
//        }
//
//        let trainingVideoOffsetY = theAttributes.frame.minY
//        if offsetY >= trainingVideoOffsetY {
//            channelVideoListView.mockHeaderView.isHidden = false
//            channelVideoListView.showShadow()
//        } else {
//            channelVideoListView.mockHeaderView.isHidden = true
//            channelVideoListView.hideShadow()
//        }
//
//        // "offsetY + 1" due to scrollToItem will not trigger currentIndexPath unless offset 1 point more
//        if let currentIndexPath = channelVideoListView.collectionView.indexPathForItem(at: CGPoint(x: view.frame.width / 2, y: offsetY + switchViewHeight + 1)),
//            currentIndexPath.row >= tabRow
//        {
//            let relatedCourseIndexRow = mChannelVideoListViewModel.channelVideoListCellViewTypeList.value.count - mChannelVideoListViewModel.relatedCourseCount - 1 // -1 for title
//            if currentIndexPath.row < relatedCourseIndexRow {
//                channelVideoListView.selectedItem(isTrainingVideo: true)
//            } else {
//                channelVideoListView.selectedItem(isTrainingVideo: false)
//            }
//        }
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension WonderciseChannelTopVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_: UICollectionView, layout _: UICollectionViewLayout, insetForSectionAt _: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 30, right: 0)
    }
}

extension WonderciseChannelTopVC: UICollectionViewDataSource {
    func collectionView(_: UICollectionView, numberOfItemsInSection _: Int) -> Int {
        return 15
    }

    func collectionView(_: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        if indexPath.row == 0 {
//            let cell = wonderciseChannelTopView.collectionView.dequeueReusableCell(withReuseIdentifier: "CellIdentifier.channelBannerCollectionViewCell", for: indexPath) as! ChannelBannerCollectionViewCell
//            topView = cell
//            return cell
//        } else if indexPath.row == 1 {
//            let cell = wonderciseChannelTopView.collectionView.dequeueReusableCell(withReuseIdentifier: "CellIdentifier.channelDescriptionCollectionViewCell", for: indexPath) as! ChannelDescriptionCollectionViewCell
//            return cell
//        } else if indexPath.row == 2 {
//            let tab = wonderciseChannelTopView.collectionView.dequeueReusableCell(withReuseIdentifier: "CellIdentifier.channelTabCollectionViewCell", for: indexPath) as! ChannelTabCollectionViewCell
//            return tab
//        } else if indexPath.row == 3 {
//            let cell = wonderciseChannelTopView.collectionView.dequeueReusableCell(withReuseIdentifier: "CellIdentifier.videosTitleCollectionViewCell", for: indexPath) as! VideosTitleCollectionViewCell
//            return cell
     //   } else {
            let cell = wonderciseChannelTopView.collectionView.dequeueReusableCell(withReuseIdentifier: "CellIdentifier.channelVideoCollectionViewCell", for: indexPath) as! ChannelVideoCollectionViewCell
            return cell
      //  }
    }
}

//
//  ChannelTabView.swift
//  WonderCoreFit
//
//  Created by charles on 2019/6/3.
//  Copyright © 2019 WonderCore. All rights reserved.
//

import UIKit

class ChannelTabCollectionViewCell: UICollectionViewCell {
    @IBOutlet var relatedCourseSectionView: UIView!
    @IBOutlet var trainingVideoButton: UIButton!
    @IBOutlet var trainingVideoBottomView: UIView!
    @IBOutlet var relatedCourseButton: UIButton!
    @IBOutlet var relatedCourseBottomView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    // for autolayout
    private lazy var width: NSLayoutConstraint = {
        let width = contentView.widthAnchor.constraint(equalToConstant: bounds.size.width)
        width.isActive = true
        return width
    }()

    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        width.constant = bounds.size.width
        return contentView.systemLayoutSizeFitting(CGSize(width: targetSize.width, height: 1))
    }

    func showShadow() {
        guard layer.shadowOpacity != 0.7 else { return }
        UIView.animate(withDuration: 0.2, animations: {
            self.layer.shadowOpacity = 0.7
        })
    }

    func hideShadow() {
        guard layer.shadowOpacity != 0.0 else { return }
        layer.shadowOpacity = 0.0
    }

    func selectedItem(isTrainingVideo: Bool) {
        if isTrainingVideo {
            guard trainingVideoBottomView.isHidden else { return }
            trainingVideoBottomView.isHidden = false
            relatedCourseBottomView.isHidden = true
//            trainingVideoButton.setTitleColor(AppColor.popular, for: .normal)
            relatedCourseButton.setTitleColor(.white, for: .normal)
        } else {
            guard !trainingVideoBottomView.isHidden else { return }
            trainingVideoBottomView.isHidden = true
            relatedCourseBottomView.isHidden = false
            trainingVideoButton.setTitleColor(.white, for: .normal)
//            relatedCourseButton.setTitleColor(AppColor.popular, for: .normal)
        }
    }
}

// MARK: - Private Methods

private extension ChannelTabCollectionViewCell {
    func setupView() {
        trainingVideoButton.setTitle("wondercise_training_video_list_tab_button1".localized, for: .normal)
        relatedCourseButton.setTitle("wondercise_training_video_list_tab_button2".localized, for: .normal)
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0
        layer.shadowOffset = CGSize(width: 0, height: 2.0)
        layer.shadowRadius = 2
    }
}

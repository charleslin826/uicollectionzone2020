//
//  WonderciseChannelTopView.swift
//  UICollectionZone
//
//  Created by Charlie Lam on 2020/9/9.
//  Copyright © 2020 CharlesLin. All rights reserved.
//

import SnapKit
import UIKit

class WonderciseChannelTopView: UIView {
    @IBOutlet var customNavigationView: UIView!
    @IBOutlet var collectionView: UICollectionView! {
        didSet {
            let flowLayout = UICollectionViewFlowLayout()
            flowLayout.minimumLineSpacing = 0
            flowLayout.estimatedItemSize = CGSize(width: UIScreen.main.bounds.width, height: 381) // if coverImageHeight - customNaviHeight = 381  will cause auto-layout conflict, therefore, slightly change estimated height to 380 base on width-375 design guideline.
            collectionView.collectionViewLayout = flowLayout
        }
    }
    
    override func awakeFromNib() {
        setupView()
    }
}

// MARK: - Private Methods

private extension WonderciseChannelTopView {
    func setupView() {
    }
}




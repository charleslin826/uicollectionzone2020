//
//  ChannelDescriptionCollectionViewCell.swift
//  WonderCoreFit
//
//  Created by Charlie Lam on 2020/8/26.
//  Copyright © 2020 WonderCore. All rights reserved.
//

import Foundation
import UIKit

class ChannelDescriptionCollectionViewCell: UICollectionViewCell {
    @IBOutlet var previewBtn: UIButton!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var greyBar: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }

    // for autolayout
    private lazy var width: NSLayoutConstraint = {
        let width = contentView.widthAnchor.constraint(equalToConstant: bounds.size.width)
        width.isActive = true
        return width
    }()

    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        width.constant = bounds.size.width
        return contentView.systemLayoutSizeFitting(CGSize(width: targetSize.width, height: 1))
    }

//    func configure(_ vm: ChannelVideoListViewModel.DescriptionCellModel) {
//        let paragraph = NSMutableParagraphStyle()
//        paragraph.minimumLineHeight = 18
//        paragraph.maximumLineHeight = 18
//        let paragraphAttributes = [NSAttributedString.Key.paragraphStyle: paragraph]
//        descriptionLabel.attributedText = NSAttributedString(string: vm.description, attributes: paragraphAttributes)
//    }
}

// MARK: - Private Methods

private extension ChannelDescriptionCollectionViewCell {
    func setupView() {
        previewBtn.setTitle("wondercise_training_video_list_preview_btn".localized, for: .normal)
        previewBtn.layer.cornerRadius = 21
    }
}

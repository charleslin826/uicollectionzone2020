//
//  ChannelVideoCollectionViewCell.swift
//  WonderCoreFit
//
//  Created by Charlie Lam on 2019/11/8.
//  Copyright © 2019 WonderCore. All rights reserved.
//

import UIKit
//import Kingfisher

class ChannelVideoCollectionViewCell: UICollectionViewCell {
//    @IBOutlet var whiteView: UIView!
//    @IBOutlet var defaultImageView: UIImageView!
//    @IBOutlet var videoImageView: UIImageView!
//    @IBOutlet var videoName: UILabel!
//    @IBOutlet var levelLbl: UILabel!
//    @IBOutlet var levelRating: LevelView!
//    @IBOutlet var divider: UIView!
//    @IBOutlet var videoTime: UILabel!
//    @IBOutlet var moreBtn: UIButton!
//    @IBOutlet var shadowView: UIView!
//    @IBOutlet var vipLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
//        vipLabel.layer.cornerRadius = 6
    }

    override func prepareForReuse() {
//        defaultImageView.isHidden = false
    }
    
    // for autolayout
    private lazy var width: NSLayoutConstraint = {
        let width = contentView.widthAnchor.constraint(equalToConstant: bounds.size.width)
        width.isActive = true
        return width
    }()

    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        width.constant = bounds.size.width
        return contentView.systemLayoutSizeFitting(CGSize(width: targetSize.width, height: 1))
    }

//    func configure(_ vm: ExploreFavoriteViewModel.FavoriteVideoItem) {
//        let imageURL = vm.image
//        settingImage(imageURL)
//        videoName.text = vm.name
//        levelRating.rating = vm.strength
//        vipLabel.isHidden = true
//        shadowView.isHidden = true
//        videoTime.text = FORMAT_TIME(second: vm.lengthInSecond)
//    }

//    func configure(_ vm: VideoCellModel) {
//        let imageURL = vm.imageUrl
//        settingImage(imageURL)
//        videoName.text = vm.name
//        levelRating.rating = vm.strength
//        if vm.isFree {
//            vipLabel.isHidden = true
//            shadowView.isHidden = true
//        } else {
//            vipLabel.isHidden = false
//            shadowView.isHidden = false
//        }
//        videoTime.text = FORMAT_TIME(second: vm.fileLengthInSeconds)
//    }
}

// MARK: - Private Methods

private extension ChannelVideoCollectionViewCell {
    func setupView() {
//        vipLabel.text = "wondercise_training_vip_title".localized
//        levelLbl.text = "wondercise_common_unit_training_strength".localized
//        shadowView.layer.cornerRadius = 6
//        shadowView.layer.shadowColor = UIColor.black.cgColor
//        shadowView.layer.shadowOpacity = 0.3
//        shadowView.layer.shadowOffset = CGSize(width: 1, height: 1)
//        shadowView.layer.shadowRadius = 2
//        whiteView.layer.shadowColor = #colorLiteral(red: 0.1921568627, green: 0.1921568627, blue: 0.1921568627, alpha: 0.78)
//        whiteView.layer.shadowOpacity = 0.78
//        whiteView.layer.shadowOffset = CGSize(width: 1, height: 2)
//        whiteView.layer.shadowRadius = 2
    }

    func settingImage(_ imgURL: String) {
        if let imageUrl = URL(string: imgURL) {
            let imageId = "\(imgURL)_thumb"
//            AppUtility.fetchImage(imageId: imageId, url: imageUrl) { hasResult, resultImage in
//                if hasResult, let unwrappedImage = resultImage {
//                    self.videoImageView.image = unwrappedImage
//                    self.defaultImageView.isHidden = true
//                }
//            }
        }
    }
}

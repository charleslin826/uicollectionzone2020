//
//  AppConstant.swift
//  UICollectionZone
//
//  Created by Charlie Lam on 2020/9/9.
//  Copyright © 2020 CharlesLin. All rights reserved.
//

import Foundation
import UIKit


func getDp(_ zeplinDp: CGFloat) -> CGFloat {
    getDpByZeplinWidth375(zeplinDp)
}

func getDpHeight(_ zeplinDp: CGFloat) -> CGFloat {
    zeplinDp * UIScreen.main.fixedCoordinateSpace.bounds.height / 667
}

func getDpByZeplinWidth750(_ zeplinDp: CGFloat) -> CGFloat {
    zeplinDp / 2
}

func getDpByZeplinWidth375(_ zeplinDp: CGFloat) -> CGFloat {
    zeplinDp * UIScreen.main.fixedCoordinateSpace.bounds.width / 375.0
}

func getDpByZeplinWidth320(_ zeplinDp: CGFloat) -> CGFloat {
    let rawValue = zeplinDp * UIScreen.main.fixedCoordinateSpace.bounds.width / 320.0
    return rawValue.rounded()
}

//
//  AppCommand.swift
//  UICollectionZone
//
//  Created by Charlie Lam on 2020/6/12.
//  Copyright © 2020 CharlesLin. All rights reserved.
//

import Foundation
import AVFoundation
import CoreFoundation
import PromiseKit
import UIKit



// MARK: - Log

func DLog(_ format: String) {
    DLog(format, NSNull() as CVarArg)
}

func DLog(_ format: String, _ args: CVarArg...) {
    #if DEVELOP
        NSLog(format, args)
    //        CLSNSLogv(format, getVaList([]))
    #elseif DEBUG
        NSLog(format, args)
    //        CLSNSLogv(format, getVaList([]))
    #else
        //        CLSLogv(format, getVaList([]))
    #endif
}


enum PermissionConstants {
    static func checkCameraPermission(vc: UIViewController) -> Guarantee<Bool> {
        Guarantee { seal in
            // The first time your app uses Camera which will automatically and asynchronously prompts the user to request authorization.
            switch AVCaptureDevice.authorizationStatus(for: AVMediaType.video) {
            case .authorized: // The user has previously granted access to the camera.
                DLog("已得到相機的授權")
                seal(true)
            case .notDetermined: // The user has not yet been asked for camera access.
                DLog("詢問相機的授權")
                AVCaptureDevice.requestAccess(for: AVMediaType.video) { granted in
                    if granted {
                        DLog("已得到相機的授權")
                        seal(true)
                    } else {
                        seal(false)
                    }
                }
            case .denied: // The user has previously denied access.
                promptAllowCameraViaSetting(vc)
                seal(false)
            case .restricted: // The user can't grant access due to restrictions.
                promptAllowCameraViaSetting(vc)
                seal(false)
            @unknown default:
                seal(false)
            }
        }
    }

    fileprivate static func promptAllowCameraViaSetting(_ vc: UIViewController) {
        DLog("沒有得到相機的授權")
        let alert = UIAlertController(title: "wondercise_camera_permission_title".localized, message: "wondercise_camera_permission_content".localized, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "wondercise_common_okbtn".localized, style: .default, handler: { _ in
            navigateToSetting()
        })
        let cancelAction = UIAlertAction(title: "wondercise_common_cancelbtn".localized, style: .cancel)
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        vc.present(alert, animated: true, completion: nil)
    }

    static func navigateToSetting() {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { success in
                DLog("Settings opened: \(success)")
            })
        }
    }
}
